// vue.config.js
const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  lintOnSave: false,
  devServer: {
    host: "localhost",
  	port: 8080, // 端口号
  	https: false, // https:{type:Boolean}
  	open: true, //配置自动启动浏览器
  	proxy: {
  		'/admin': { //代理api
  			// target: 'http://ceshi5.dishait.cn/admin',//服务器api地址
			target: 'http://www.niya.cn/admin',
  			ws: true,// proxy websockets
  			changeOrigin: true,//是否跨域
  			pathRewrite: { //重写路径
  				'^/admin': ''
  			}
  		}
  	}
  },
  chainWebpack(config) {
      // set svg-sprite-loader
      config.module
        .rule("svg")
        .exclude.add(resolve("src/icons"))
        .end();
      config.module
        .rule("icons")
        .test(/\.svg$/)
        .include.add(resolve("src/icons"))
        .end()
        .use("svg-sprite-loader")
        .loader("svg-sprite-loader")
        .options({
          symbolId: "icon-[name]"
        })
        .end();
    }
}