export default {
	cate:[{
		name:"新品",
		src:"https://res.vmallres.com/pimages/product/6901443331376/428_428_FAF5BBAB67C16D7426B5B1A2A38F9001DED6D011A0EE9977mp.png",
	},{
		name:"华X手机",
		src:"https://res.vmallres.com/pimages/product/6901443331116/428_428_53700717ABF3AC923E6F287B643C236E7732FD9C964FAE1Bmp.png",
	},{
		name:"荣X手机",
		src:"https://res.vmallres.com/pimages/product/6901443352555/428_428_5B02682D327402A4774F83804F79043939BAE5C612C15A6Emp.png",
	},{
		name:"智慧屏",
		src:"https://res.vmallres.com/pimages/product/6901443331093/428_428_21EEE8FC1E9B970CAA527CC00F668B933528C62FBC4CD1E4mp.png",
	},{
		name:"笔记本",
		src:"https://res.vmallres.com/pimages/product/6901443313112/428_428_1563869539329mp.png",
	},{
		name:"平板",
		src:"https://res.vmallres.com/pimages/product/6901443265206/428_428_1539243364035mp.png",
	},{
		name:"智能穿戴",
		src:"https://res.vmallres.com/pimages/product/6901443330133/428_428_F37FD45455E361B4C96E32969091FE564C1E7207EC2A4737mp.png",
	},{
		name:"智能家居",
		src:"https://res.vmallres.com/pimages/common/mobile/frontCategory/lezwuz17u1roxh8n5zro.png",
	},{
		name:"专属配件",
		src:"https://res.vmallres.com/pimages/common/mobile/frontCategory/lezwuz17u1roxh8n5zro.png",
	},{
		name:"通用配件",
		src:"https://res.vmallres.com/pimages/pages/mobile/frontCategory/j6oZEBIMDz2yM9qJaNwX.jpg",
	},{
		name:"生态产品",
		src:"https://res.vmallres.com/pimages/common/mobile/frontCategory/1520582668647.jpg",
	}],
	list(){
		var list = []
		for (let i = 0; i < this.cate.length; i++) {
			list.push({
				list:[]
			})
		}
		for (let i = 0; i < this.cate.length; i++) {
			for (let j = 0; j < 24; j++) {
				list[i].list.push({ 
					src:this.cate[i].src,
					name:this.cate[i].name,
				})
			}
		}
		return list
	}
}