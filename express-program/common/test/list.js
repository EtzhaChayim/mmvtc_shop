export default {
	list: [{
			title: "空气感超薄苹果手机壳",
			titlepic: "https://yanxuan-item.nosdn.127.net/5984928facd569ef906becba3469810b.png?imageView&quality=65&thumbnail=330x330",
			desc: "0.4mm纸一样薄  磨砂手感",
			pprice: 9.9,
			comment_num: 100,
			good_num: "98%"
		}, {
			title: "3D全屏高清手机玻璃膜",
			titlepic: "https://yanxuan-item.nosdn.127.net/8a3b42e4956d8d00e40ddd4a7c470452.png?imageView&quality=65&thumbnail=330x330",
			desc: "进口材料，偏执工艺，玻璃潮流！",
			pprice: 49,
			comment_num: 360,
			good_num: "98%"
		},
		{
			title: "三合一磁吸车载支架",
			titlepic: "https://yanxuan-item.nosdn.127.net/765a18a017acf0b5c012ce765696be6f.png?imageView&quality=65&thumbnail=330x330",
			desc: "带有隐形安全锤和割刀的手机支架！新增香薰版本！",
			pprice: 77,
			comment_num: 569,
			good_num: "98%"
		},
		{
			title: "3D全屏高清手机玻璃膜（安卓版）",
			titlepic: "https://yanxuan-item.nosdn.127.net/575977783cdaf86f36c41df05bd9885b.png?imageView&quality=65&thumbnail=330x330",
			desc: "全新安卓版手机膜闪耀上市",
			pprice: 36,
			comment_num: 321,
			good_num: "98%"
		},
		{
			title: "3D全屏高清手机玻璃膜（安卓版）",
			titlepic: "https://yanxuan-item.nosdn.127.net/eb5a2d258905e0a6096af057946b0bf0.png?imageView&quality=65&thumbnail=330x330",
			desc: "进口液态硅胶，磁吸支架良配！",
			pprice: 46,
			comment_num: 235,
			good_num: "98%"
		},
		{
			title: "云感手机保护壳",
			titlepic: "https://yanxuan-item.nosdn.127.net/e5cbf06f9e4c1387e81746b6bc2e3fbb.png?imageView&quality=65&thumbnail=330x330",
			desc: "日本进口液态硅胶，iPhone手机的舒适感。",
			pprice: 59,
			comment_num: 132,
			good_num: "98%"
		},
		{
			title: "云感手机保护壳（轻薄款）",
			titlepic: "https://yanxuan-item.nosdn.127.net/4242624adafaf02879eaea84d19c51e4.png?imageView&quality=65&thumbnail=330x330",
			desc: "轻薄款闪耀登场，亲肤，柔软，防摔",
			pprice: 59,
			comment_num: 132,
			good_num: "98%"
		},
		{
			title: "钢化玻璃手机壳",
			titlepic: "https://yanxuan-item.nosdn.127.net/2bff5cf58876727b2051b1670c851d73.png?imageView&quality=65&thumbnail=330x330",
			desc: "日本进口液态硅胶，iPhone手机的舒适感。",
			pprice: 59,
			comment_num: 132,
			good_num: "98%"
		},
		{
			title: "颐和园六合太平国风系列手机壳",
			titlepic: "https://yanxuan-item.nosdn.127.net/59d5ec67b7429c023fd21ac81c1b352f.png?imageView&quality=65&thumbnail=330x330",
			desc: "瑞兽仙羽，宝物加持",
			pprice: 59,
			comment_num: 132,
			good_num: "98%"
		}
	],
	// 获取随机数据
	getList(total = 10){
		var arr = []
		for (var i = 0; i < total; i++) {
			var index = this.rnd(0,this.list.length - 1)
			arr.push(this.list[index])
		}
		return arr
	},
	rnd(n, m){
		return Math.floor(Math.random()*(m-n+1)+n);
	}
}
