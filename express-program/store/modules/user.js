export default {
	state:{
		userinfo:false,
		token:false,
		loginStatus:false
	},
	mutations:{
		// 初始化数据
		initUser(state){
			let userinfo = uni.getStorageSync('userinfo')
			if (userinfo) {
				userinfo = JSON.parse(userinfo)
				
				state.userinfo = userinfo
				state.token = userinfo.token
				state.loginStatus = true
			}
		},
		// 登录
		login(state,userinfo){
			state.userinfo = userinfo
			state.token = userinfo.token
			state.loginStatus = true
			uni.setStorageSync('userinfo',JSON.stringify(userinfo))
		},
		// 退出
		logout(state){
			state.token = false
			state.userinfo = false
			state.loginStatus = false
			uni.removeStorageSync('userinfo')
		}
	},
	actions:{
		
	}
}